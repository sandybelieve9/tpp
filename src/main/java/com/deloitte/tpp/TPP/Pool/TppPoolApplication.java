package com.deloitte.tpp.TPP.Pool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TppPoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(TppPoolApplication.class, args);
	}

}
